package studentCoursesBackup.myTree;

/**
 * ObserverI Interface .
 */
public interface ObserverI 
{
	
	/**
	 * Node update.
	 *
	 * @param del_course the course name
	 */
	public void node_update(String del_course);
}
