package studentCoursesBackup.myTree;

/**
 * SubjectI Interface.
 */
public interface SubjectI 
{
	
	/**
	 * Node register.
	 *
	 * @param t_node the node to be registered as observer
	 */
	public void node_register(Node t_node);
	
	/**
	 * Node deregister.
	 *
	 * @param t_node the node to be unregistered
	 */
	public void node_deregister(Node t_node);
	
	/**
	 * Notify all.
	 *
	 * @param del_course the course name
	 */
	public void notifyAll(String del_course);
}
