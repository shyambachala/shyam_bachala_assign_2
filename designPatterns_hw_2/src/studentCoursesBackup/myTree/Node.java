package studentCoursesBackup.myTree;
import java.util.ArrayList;
import studentCoursesBackup.util.Results;

/**
 * Class Node
 * Interface Implementations: Cloneable, Subjecti, ObserverI
 */
public class Node implements Cloneable, SubjectI, ObserverI
{
	/** The node observers. */
	private ArrayList<Node> node_observers;
	
	/** The root node. */
	private Node root_node = null;
	
	/** The node zero. */
	Node node_zero = null;
	
	/** The node one. */
	Node node_one = null;
	
	/** The node b number. */
	int node_b_number = 0;

	/** The node course names. */
	ArrayList<String> node_course_names ;

	/** The node concat print. */
	private String node_concat_print = "";
	
	/**
	 * New node initialization.
	 *
	 * @param number -> b_number
	 * @param c_name -> course_name
	 */
	public Node(int number, String c_name) 
	{
		node_course_names = new ArrayList<String>();
		node_observers = new ArrayList<Node>();
		node_b_number = number;
		node_course_names.add(c_name);
	}
	
	/* 
	 * Register Node as an Observer
	 * @param t_node -> observer node
	 */
	public void node_register(Node t_node)
	{
		try
		{
			node_observers.add(t_node);
		}
		catch(Exception e)
		{
			System.out.println("\nException : NumberPointerException\n");
			e.printStackTrace();
		}
		finally
		{}
	}
	
	/* De-register Node as an Observer
	 * @param t_node -> observer node
	 */
	public void node_deregister(Node t_node)
	{
		try
		{
			node_observers.remove(t_node);
		}
		catch(Exception e)
		{
			System.out.println("\nException : NullPointerException\n");
			e.printStackTrace();
		}
		finally
		{}
	}
	
	/* Notify all Nodes abou the change
	 * @param del_course -> course name
	 */
	public void notifyAll(String del_course)
	{
		if((!node_observers.isEmpty()) && (del_course != null))
		{
			for(Node t_node: node_observers)
				t_node.node_update(del_course);
		}
	}
	
	/* Update Observer
	 * @param del_course -> course name
 	 */
	public void node_update(String del_course)
	{
		node_course_names.remove(del_course);
	}
	
	/* 
	 * Clone method for creating a copy of the original node
	 */
	public Object clone() throws CloneNotSupportedException
	{
		Node t = (Node)super.clone();
		ArrayList<String> tu = new ArrayList<String>(t.node_course_names);
		t.node_course_names = new ArrayList<String>(tu);
		return t;
	}
	
	/**
	 * Prints the nodes.
	 *
	 * @param r the Results instance
	 */
	public void printNodes(Results r)
	{
		Node r_n = getRoot_node();
		r.setResults(r_n.get_concat_print());
	}
	
	/**
	 * Node insert.
	 *
	 * @param n_node the new node
	 * @param b_number the b_number
	 */
	public void node_insert(Node n_node, int b_number)//, String course)
	{
			Node ins_node_ptr = node_insert_traversal(b_number);	
			if(ins_node_ptr != null)
			{
				if(b_number < ins_node_ptr.node_b_number)
					ins_node_ptr.node_zero = n_node;
				else
					ins_node_ptr.node_one = n_node;
			}
	}
	
	/**
	 * Node insert traversal.
	 *
	 * @param s_node_number the b_number
	 * @return the parent node
	 */
	private Node node_insert_traversal(int s_node_number)
	{
		Node t_node = getRoot_node();
		Node p_node = null;
		while(t_node != null)
		{
			if(s_node_number < t_node.node_b_number)
			{
				p_node = t_node;
				t_node = t_node.node_zero;
			}
			else
			{
				p_node = t_node;
				t_node = t_node.node_one;
			}
		}
		return p_node;
	}
	
	/**
	 * Node add course.
	 *
	 * @param b_number the b_number
	 * @param course the course name
	 */
	public void node_add_course(int b_number, String course)
	{
		Node ins_node_ptr = node_add_course_traversal(b_number);	
		if(ins_node_ptr != null)
		{
			ins_node_ptr.node_course_names.add(course);
		}
	}
	
	/**
	 * Node add course traversal.
	 *
	 * @param s_node_number the b_number
	 * @return the node for which the new course is to be added
	 */
	private Node node_add_course_traversal(int s_node_number)
	{
		Node t_node = getRoot_node();
		Node p_node = null;
		while(t_node != null)
		{
			if(s_node_number < t_node.node_b_number)
			{
				p_node = t_node;
				t_node = t_node.node_zero;
			}
			else if(s_node_number == t_node.node_b_number)
			{
				return t_node;
			}
			else
			{
				p_node = t_node;
				t_node = t_node.node_one;
			}
		}
		return p_node;
	}
	
	/**
	 * Node delete course.
	 *
	 * @param b_number the b_number
	 * @param course the course name
	 */
	public void node_delete_course(int b_number, String course)
	{
		Node ins_node_ptr = node_delete_course_traversal(b_number);
		if(ins_node_ptr != null)
		{
			ins_node_ptr.node_course_names.remove(course);
			ins_node_ptr.notifyAll(course);
		}
	}

	/**
	 * Node delete course traversal.
	 *
	 * @param s_node_number the b_number
	 * @return the parent node of the node for which course updation is to be done
	 */
	private Node node_delete_course_traversal(int s_node_number)
	{
		Node t_node = getRoot_node();
		Node p_node = null;
		while(t_node != null)
		{
			if(s_node_number < t_node.node_b_number)
			{
				p_node = t_node;
				t_node = t_node.node_zero;
			}
			else if(s_node_number == t_node.node_b_number)
			{
				return t_node;
			}
			else
			{
				p_node = t_node;
				t_node = t_node.node_one;
			}
		}
		return p_node;
	}
	
	/**
	 * Tree print.
	 */
	public void tree_print()
	{
		tree_print_sub(getRoot_node());
	}
	
	/**
	 * Tree print sub.
	 *
	 * @param t_node the Node to be traversed and printed
	 */
	private void tree_print_sub(Node t_node) 
	{
		if(t_node != null)
		{
			String temp_courses = "";
			tree_print_sub(t_node.node_zero);
			for(String t: t_node.node_course_names)
			{
				temp_courses = temp_courses + " " + t;
			}
			Node r_n = getRoot_node();
			r_n.node_concat_print = r_n.node_concat_print + "B_Number: " + t_node.node_b_number + " --> Courses: " + temp_courses + "\n";
			tree_print_sub(t_node.node_one);
		}
	}
	
	/**
	 * Gets the concat print.
	 *
	 * @return the concat print
	 */
	public String get_concat_print()
	{
		return node_concat_print;
	}
	
	/**
	 * Sets the concat print.
	 */
	public void set_concat_print()
	{
		node_concat_print = "";
	}

	/**
	 * Gets the root node.
	 *
	 * @return the root node
	 */
	public Node getRoot_node() {
		return root_node;
	}

	/**
	 * Sets the root node.
	 *
	 * @param r_node the new root node
	 */
	public void setRoot_node(Node r_node) {
		this.root_node = r_node;
	}
}
