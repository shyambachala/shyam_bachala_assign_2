package studentCoursesBackup.driver;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.util.FileProcessor;
import studentCoursesBackup.util.Results;

/**
 * The Driver Class.
 */
public class Driver 
{
	
	/**
	 * Main method.
	 *
	 * @param args the command-line arguments
	 * @throws IOException Signals when an I/O exception occurrs.
	 */
	public static void main(String[] args) throws IOException
	{
//		COMMAND LINE ARGUMENTS CHECKING
		if(args.length != 5)
		{
			System.out.println("INVALID NUMBER OF INPUT PARAMETERS: " + args.length + "\nGIVE INPUT AS BELOW\n"
					+ "ant -buildfile src/build.xml run -Darg0=/home/input_file/input.txt -Darg1=/home/output_file/delete.txt -Darg2=/home/output_file/output1.txt "
					+ "-Darg3=/home/output_file/output2.txt -Darg4=/home/output_file/output3.txt\r\n");
			System.exit(4);
		}
		
//		LOCAL VARIABLES
		ArrayList<Integer> tree_num_list = new ArrayList<Integer>();
		String inputLine, deleteLine;
		boolean root_checker = false;
		File infile = new File(args[0]);
		File delfile = new File(args[1]);
		File outfile1 = new File(args[2]);
		File outfile2 = new File(args[3]);
		File outfile3 = new File(args[4]);

//		CHECKING INPUT.TXT IS EMPTY
		if(infile.length() == 0)
		{
			System.out.println("ERROR: input.txt IS EMPTY");
			System.exit(2);
		}
//		CHECKING DELETE.TXT IS EMPTY
		if(delfile.length() == 0)
		{
			System.out.println("ERROR: delete.txt IS EMPTY");
			System.exit(3);
		}		
		
//		CREATION OF THE TREES AND INSERTING THE B_NUMBER AND THE COURSES INTO THE TREE USING THE FILE READING
		FileProcessor freader = new FileProcessor(infile, true);    
		Node root_node = null;
		Node backup_root_node_1 = null;
		Node backup_root_node_2 = null;
			
	    while((inputLine = freader.readLine()) != null)
	    {
	      	System.out.println(inputLine);
	       	String[] node_inputs = inputLine.split(":");
//	       	CHECK FOR COURSE NAME
	       	if(node_inputs.length == 1)
	       	{
	       		continue;
	       	}
//	       	CHECK WHETHER THE COURSE NAME IS VALID
	       	if(!verify_course(node_inputs[1]))
	       	{
	       		continue;
	       	}
//	       	CHECKER FOR ROOT IS CREATED OR NOT 
	       	if(!root_checker)
	       	{
	       		try
	       		{
	       			root_node = new Node(Integer.parseInt(node_inputs[0]), node_inputs[1]);
	       			
	       			//BACKUP 1
	       			backup_root_node_1 = (Node)root_node.clone();
	       			backup_root_node_1.setRoot_node(backup_root_node_1);
	       			
	       			//BACKUP 2
	       			backup_root_node_2 = (Node)root_node.clone();
	       			backup_root_node_2.setRoot_node(backup_root_node_2);
	       			
	       			root_node.setRoot_node(root_node);
	       			root_node.node_register(backup_root_node_1);
	       			root_node.node_register(backup_root_node_2);
	       			
	       			root_checker = true;
	       			tree_num_list.add(Integer.parseInt(node_inputs[0]));
	       		}
	       		catch(Exception e)
	       		{
	       			System.out.println("\nException : NumberFormatException\n");
	       			e.printStackTrace();
	       		}
	       		finally
	       		{}
	       	}
	       	else
	       	{
	       		try
	       		{
	       			boolean check_condition = tree_num_list.contains(Integer.parseInt(node_inputs[0]));
//	       			CHECK IF NODE FOR THE b_NUMBER IS ALREADY CREATED OR NOT
	       			if(!check_condition)
	       			{
	       				tree_num_list.add(Integer.parseInt(node_inputs[0]));
	       				Node n_node = new Node(Integer.parseInt(node_inputs[0]), node_inputs[1]);
	       				Node bu_n_node_1 = (Node)n_node.clone();
	       				Node bu_n_node_2 = (Node)n_node.clone();
	       				
	       				n_node.node_register(bu_n_node_1);
	       				n_node.node_register(bu_n_node_2);
	       				
	       				root_node.node_insert(n_node, Integer.parseInt(node_inputs[0]));
	       				backup_root_node_1.node_insert(bu_n_node_1, Integer.parseInt(node_inputs[0]));
	       				backup_root_node_2.node_insert(bu_n_node_2, Integer.parseInt(node_inputs[0]));
	       			}
//	       			IF NODE ALREADY PRESENT ADD THE COURSE TO THE CORRESPONDING NODE
	       			if(check_condition) 
	       			{
	       				root_node.node_add_course(Integer.parseInt(node_inputs[0]), node_inputs[1]);
	       				backup_root_node_1.node_add_course(Integer.parseInt(node_inputs[0]), node_inputs[1]);
	       				backup_root_node_2.node_add_course(Integer.parseInt(node_inputs[0]), node_inputs[1]);
	       			}
	       		}
	       		catch(Exception e)
	       		{
	       			System.out.println("\nException : NumberFormatException\n");
	       			e.printStackTrace();
	       		}
	       		finally
	       		{}
	       	}
	    }  
//		REMOVE THE FILE STREAMS
	    freader.remove_dev();
//	    GENERATING THE OUTPUT FILES USING THE RESULTS INSTANCES
	    System.out.println("\n\n*********ORI TREE*****************");
	    root_node.tree_print();
	    System.out.println(root_node.get_concat_print());
	    System.out.println("*********************************\n\n");
	        
	    System.out.println("\n\n*********BU 1*********************");
	    backup_root_node_1.tree_print();
	    System.out.println(backup_root_node_1.get_concat_print());
	    System.out.println("*********************************\n\n");
	        
	    System.out.println("\n\n*********BU 2*********************");
	    backup_root_node_2.tree_print();
	    System.out.println(backup_root_node_2.get_concat_print());
	    System.out.println("*********************************\n\n");
	        
//	    FILE READING FOR THE DELETE.TXT IS INITIALIZED AND THE CORRESPONDING VALID COURSE NAMES ARE DELETED FOR THE NODES AND THE INTERNALLY NOTIFYALL IS CALLED 
//	    FROM THE CORRESSPONDING ORIGINAL NODE TO THE BACKUP NODES
		FileProcessor delreader = new FileProcessor(delfile, true);
		while((deleteLine = delreader.readLine()) != null)
	    {
	    	System.out.println(deleteLine);
	       	String[] node_deletes = deleteLine.split(":");
	       	int input_arg0 = Integer.parseInt(node_deletes[0]);	    
	       	if(node_deletes.length == 1)
	       	{
	       		continue;
	       	}
	       	String input_arg1 = node_deletes[1];	      	
	       	if(!tree_num_list.contains(input_arg0))
	       	{
	       		continue;
	       	}	
	       	if(!verify_course(input_arg1))
	       	{
	       		continue;
	       	}
	       	try
	       	{
	       		root_node.node_delete_course(input_arg0, input_arg1);
	       	}
	       	catch(Exception e)
	       	{
	       		System.out.println("\nException : NumberFormatException\n");
	       		e.printStackTrace();
	       	}
	       	finally
	       	{}	
       }  
       delreader.remove_dev();
	        
//	    GENERATING THE OUTPUT FILES USING THE RESULTS INSTANCES
	   System.out.println("\n\n*********ORI TREE*****************");
	   root_node.set_concat_print();
	   root_node.tree_print();
	       
	   Results results_root_node_orig = new Results();
	   root_node.printNodes(results_root_node_orig);
	   results_root_node_orig.writeToFile(outfile1);
	        
	   System.out.println(root_node.get_concat_print());
	   System.out.println("*********************************\n\n");
	        
	   System.out.println("\n\n*********BU 1*********************");
	   backup_root_node_1.set_concat_print();
	   backup_root_node_1.tree_print();
	       
	   Results results_backup_root_node_1 = new Results();
	   backup_root_node_1.printNodes(results_backup_root_node_1);
	   results_backup_root_node_1.writeToFile(outfile2);
	        
	   System.out.println(backup_root_node_1.get_concat_print());
	   System.out.println("*********************************\n\n");
	        
	   System.out.println("\n\n*********BU 2*********************");
	   backup_root_node_2.set_concat_print();
	   backup_root_node_2.tree_print();
	        
	   Results results_backup_root_node_2 = new Results();
	   backup_root_node_2.printNodes(results_backup_root_node_2);
	   results_backup_root_node_2.writeToFile(outfile3);
	        
	   System.out.println(backup_root_node_2.get_concat_print());
	   System.out.println("*********************************\n\n");    
	}
	
	/**
	 * Verify whether it is valid course.
	 *
	 * @param node_c the course name
	 * @return true, if successful
	 */
	public static boolean verify_course(String node_c)
	{
		if(node_c.contains("A") || node_c.contains("B") || node_c.contains("C") || node_c.contains("D") || node_c.contains("E") || node_c.contains("F") || node_c.contains("G") || node_c.contains("H") || node_c.contains("I") || node_c.contains("J") || node_c.contains("K"))
			return true;
		else
			return false;
	}
}
