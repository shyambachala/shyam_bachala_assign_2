## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile:

ant -buildfile src/build.xml all

Description: Compiles your code and generates .class files inside the BUILD
folder.

-----------------------------------------------------------------------

## To run by code:

ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=delete.txt -Darg2=output1.txt -Darg3=output2.txt -Darg4=output3.txt

Description:

Testing the input.txt file and delete.txt has been done by placing it in the [shyam_bachala_assign_2/studentCoursesBackup]
as mentioned in the design requirements.

-------------------------------------------------------------------------
## To create tarball for submission

ant -buildfile src/build.xml tarzip

-------------------------------------------------------------------------

## Choice of Data Structures

1. Binary Search Tree is easy to implement where the values are sent into the left branch if the value is greater than the value at the node,
   sent to right branch if the value is greater. 

2. In BST in an average case, if we want to search or insert or delete a value, it atmost takes minimum of 'n' traversals where n is the height of the tree.
   This significantly reduces the number of comparisions if the dataset is large. In Big O notation is O(n) where n = log(number of elements in tree).

3. Using the BST, the nodes can be printed in the sorted manner by performing an inorder traversal on the tree. This quite a characteristic
   as many other data structures have to implement some type of sorting algorithms to do that.

4. If we want to search for a set of nodes in a range it is easier using BST as it is like a sorted array. Therefore the time complexity is
   is less and is more efficient.

----------------------------------------------------------------------------------------------------------------------
